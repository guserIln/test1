from loader import dp
from aiogram import types
import requests

from utils.Shifr import Shifr


@dp.message_handler()
async def getMessage(message: types.Message):
    if message.text.isdigit():
        length = int(message.text)
        result = Shifr(length)
        await message.answer(result.password)
        return
    res = requests.get('https://random.dog/woof.json')
    answer = res.json()
    await message.answer_photo(answer["url"])
