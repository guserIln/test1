if __name__ == '__main__':
    num = int(input("Введите число: "))
    if (num == 1):
        print('Число не является ни простым, ни составным')
        exit()
    for i in range(2, num):
        if (num % i == 0):
            print('Число составное')
            exit()
    print('Число простое')

