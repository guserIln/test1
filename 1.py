if __name__ == '__main__':
    day = input('Введите день в виде числа: ')
    month = input('Введите месяц в виде строки: ')
    temperature = input('Температура: ')

    if int(day) > 31 and int(day) <= 0:
        print('Введен неверный день месяца')
        exit()

    if month == 'январь': month = 'января'
    elif month == 'февраль': month = 'февраля'
    elif month == 'март': month = 'марта'
    elif month == 'апрель': month = 'апреля'
    elif month == 'май': month = 'мая'
    elif month == 'июнь': month = 'июня'
    elif month == 'июль': month = 'июля'
    elif month == 'август': month = 'августа'
    elif month == 'сентябрь': month = 'сентября'
    elif month == 'октябрь': month = 'октября'
    elif month == 'ноябрь': month = 'ноября'
    elif month == 'декабрь': month = 'декабря'

    print(f'Сегодня {day} {month}. На улице {temperature}  градусов')
    if (int(temperature) < 0):
        print('Холодно, лучше остаться дома')
