import uuid

from django.db import models
# Create your models here.
GENDER_CHOICE = (
   ('М', 'Мужской'),
   ('Ж', 'Женский')
)

class Patient(models.Model):
    first_name = models.CharField(max_length=255, verbose_name='Имя')
    second_name = models.CharField('Фамилия', max_length=255)
    gender = models.CharField('Пол', max_length=255, choices=GENDER_CHOICE, default=GENDER_CHOICE[0])
    birthDate = models.DateField(default='2000-01-01')

    def __str__(self):
      return self.first_name


class Visit(models.Model):
    name = models.CharField(max_length=255, verbose_name='Название')
    patient_id = models.ForeignKey('Фамилия', max_length=255)
    visitDate = models.DateField(default='2000-01-01')
    diagnos = models.ManyToManyField(max_length=255, verbose_name='Диагноз')
    def __str__(self):
      return self.name

class  Doctor(models.Model):
    first_name = models.CharField(max_length=255, verbose_name='Имя')
    second_name = models.CharField('Фамилия', max_length=255)
    gender = models.CharField('Пол', max_length=255, choices=GENDER_CHOICE, default=GENDER_CHOICE[0])
    birthDate = models.DateField(default='2000-01-01')

    def __str__(self):
      return self.first_name

