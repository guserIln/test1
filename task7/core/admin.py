from django.contrib import admin
from core import models

# Register your models here.
@admin.register(models.Student)
class Student(admin.ModelAdmin):
    list_display = ('first_name', )