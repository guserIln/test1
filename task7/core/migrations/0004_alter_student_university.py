# Generated by Django 4.2.1 on 2023-05-14 19:47

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0003_lessons_phone_university_student_date_birth_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='student',
            name='university',
            field=models.ForeignKey(blank=True, default=1, on_delete=django.db.models.deletion.CASCADE, related_name='student', to='core.university'),
        ),
    ]
