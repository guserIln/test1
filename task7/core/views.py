from django.shortcuts import render

def hello(request):
    num1 = request.POST.get('num1')
    num2 = request.POST.get('num2')

    if num1 is None:
        num1 = '0'
    if num2 is None:
        num2 = '0'

    context = {'name': request.GET.get('name'), 'result': int(num1) + int(num2) }

    return render(request, 'core/hello.html', context)
# Create your views here.
