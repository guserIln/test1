import uuid

from django.db import models

GENDER_CHOICE = (
   ('М', 'Мужской'),
   ('Ж', 'Женский')
)

class University(models.Model):
   name = models.CharField("Название",max_length=255)

   def __str__(self):
      return self.name

class Lessons(models.Model):
   name = models.CharField("Название",max_length=255)
   def __str__(self):
      return self.name
# Create your models here.
class Student(models.Model):
   #id = models.UUIDField(default=uuid.UUID, primary_key=True)
   first_name = models.CharField(max_length=255, verbose_name='Имя')
   second_name = models.CharField('Фамилия',max_length=255)
   gender = models.CharField('Пол', max_length=255, choices=GENDER_CHOICE, default=GENDER_CHOICE[0])
   is_active = models.BooleanField(default=True)
   date_birth = models.DateField(default='2000-01-01')
   dc = models.DateTimeField(blank=True, null=True)
   dm = models.DateTimeField(blank=True, null=True)
   university = models.ForeignKey( University, on_delete=models.CASCADE, related_name='student', blank=True, default=1,)
   lessons = models.ManyToManyField(Lessons)

class StudentId(models.Model):
   number = models.CharField(max_length=255, unique=True)
   student = models.OneToOneField(Student, on_delete=models.CASCADE, related_name='studentid')

   class Meta:
      verbose_name = 'Студент'
      verbose_name_plural = 'Студенты'
      ordering = ('number',)
      abstract = True

   def __str__(self):
      return f'{self.number} - {self.student_name}'

class Phone(models.Model):
   value = models.CharField(max_length=255)
   name = models.CharField(max_length=255)
   desc = models.TextField()

class FavouritePhone(Phone):
   is_favourite = models.BooleanField()





