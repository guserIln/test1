from django.db import models

# Create your models here.
class Book(models.Model):
    list_count = models.IntegerField()
    text = models.TextField()