from django.contrib import admin
from core import models

# Register your models here.
@admin.register(models.Book)
class Book(admin.ModelAdmin):
    list_display = ('id', 'text',)