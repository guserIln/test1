import math

if __name__ == '__main__':
    speed_kmh = 1079252848.8;
    speed_kms = math.trunc(1079252848.8 / 3600);
    print(f'Скорость света равна {speed_kms} км/с')